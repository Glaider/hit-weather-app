# **Weather Application** #


### What is this repository for? ###

* Weather Application Institute Java Project.
* Version 1.0

### How do I get set up? ###
1. Download and run this app using Eclipse or Intellij idea

2. 
  * Download HIT-Weather-App/WeatherGui/lib/WeatherLib.jar.
  * Include WeatherLib.jar in to your project.
  * An example for using this library:
    
```
#!java
public static  void main(String[] args)
    {
        IWeatherDataService service = null;

        try {
            service = WeatherDataServiceFactory.getOrCreateWeatherDataService(WeatherDataServiceFactory.OPENWEATHERMAP);

            if (service != null)
            {
                WeatherDataCurrentDay data = (WeatherDataCurrentDay) service.getWeatherDataCurrentDay(
                        new WeatherLocation("London","GB",null,null), IWeatherDataService.ETemperatureUnits.CELSIUS);

                /**
                 * Print Samples
                 */
                double temperature = data.getTemperature().getCurrentTempValue();
                System.out.println(
                        "Temperature in London :" + temperature + " " + IWeatherDataService.ETemperatureUnits.CELSIUS+System.lineSeparator());

                String windName = data.getWindDirection().getName();
                System.out.println(windName+System.lineSeparator());

                System.out.println(data);

            }

        } catch (WeatherDataServiceException e) {
            e.printStackTrace();
        }

    }

```