package il.ac.hit.weathergui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ThermometerPlot;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.DialTextAnnotation;
import org.jfree.chart.plot.dial.StandardDialFrame;
import org.jfree.chart.plot.dial.StandardDialRange;
import org.jfree.chart.plot.dial.StandardDialScale;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultValueDataset;
import org.xml.sax.SAXException;

import il.ac.hit.weatherdata.Time;
import il.ac.hit.weatherservice.IWeatherDataService;
import il.ac.hit.weatherservice.IWeatherDataService.ETemperatureUnits;
import il.ac.hit.weatherservice.OpenWeatherMap;
import il.ac.hit.weatherservice.WeatherDataCurrentDay;
import il.ac.hit.weatherservice.WeatherDataDailyForecast;
import il.ac.hit.weatherservice.WeatherDataServiceException;
import il.ac.hit.weatherservice.WeatherDataServiceFactory;
import il.ac.hit.weatherservice.WeatherLocation;

/**
 * The Class GUI.
 *
 * @author Stephan Sarkisyan , Alex Verevkine
 */
public class Gui extends JFrame
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The show Weather button , Show Weather Map button. */
	private final JButton showWeatherButton, webMapBtn;

	/** The content pane. */
	private final JLabel contentPane;

	/** The Units button group. */
	private final ButtonGroup buttonGroup;

	/** The Forecast Days combo box. */
	private final JComboBox<EForecastOptions> comboBox;

	/** The Forecast Days tabs panel. */
	private final JTabbedPane dayTabsPanel;

	/** The Default logo animated Image. */
	private final ImageIcon defaultLogoAnimated;

	/**
	 * The Country Label , City Label , Coordinates choose Label , Latitude
	 * Label , Longitude Label.
	 */
	private final JLabel lblCountry, lblCity, lblCoordinatesSearch, lblLat, lblLon;

	/**
	 * The Introduction Logo
	 */
	private final JTextPane logoIntro;

	/** The Metric button , Imperial button , Kelvin button . */
	private final JRadioButton rdbtnMetric, rdbtnImperial, rdbtnKelvin;

	/** The layered pane with Current weather info and Daily weather info. */
	private final JLayeredPane layeredPane;

	/** The Country check box , Search type check box. */
	private final JCheckBox countryCheckBox, searchTypeCheckBox;

	/** The Temperature unit selected. */
	private ETemperatureUnits unitSelected;

	/** The current day data. */
	private WeatherDataCurrentDay currentDayData;

	/**
	 * The current day panel , graph panel , current weather panel ,location
	 * panel.
	 */
	private JPanel currentDayPanel;

	/** Daily weather forecast temperature graph panel */
	private final JPanel graphPanel;

	/** Current weather forecast panel */
	private final JPanel currentWeatherPanel;

	/** Search location panel */
	private final JPanel locationPanel;

	/** The country name , city name. */
	private final JTextField countryName, cityName, latitudeText, longitudeText;

	/** The Daily forecast data. */
	private WeatherDataDailyForecast forcastData;

	/** The chosen forecast days value. */
	private int forcastDays;

	/** The Thermometer plot. */
	private ThermometerPlot plot;

	/** The chosen weather service. */
	private IWeatherDataService chosenService;

	/** The Daily weather forecast graph data set. */
	private List<DefaultCategoryDataset> graphDataset;

	/** Line separators in GUI */
	private final JSeparator separatorOne, seperatorTwo;

	/**
	 * Starting GUI.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		EventQueue.invokeLater(() -> {
			try
			{
				final Gui guiFrame = new Gui();
				guiFrame.startGUI();
			} catch (final Exception e)
			{
				e.printStackTrace();
			}

		});
	}

	/**
	 * Create the frame.
	 */
	public Gui()
	{

		contentPane = new JLabel();

		countryName = new JTextField();
		cityName = new JTextField();
		latitudeText = new JTextField();
		longitudeText = new JTextField();

		layeredPane = new JLayeredPane();

		separatorOne = new JSeparator();
		seperatorTwo = new JSeparator();

		dayTabsPanel = new JTabbedPane(JTabbedPane.TOP);

		graphPanel = new JPanel();
		currentWeatherPanel = new JPanel();
		countryCheckBox = new JCheckBox("");
		searchTypeCheckBox = new JCheckBox("");

		rdbtnMetric = new JRadioButton("Celsius");
		rdbtnImperial = new JRadioButton("Fahrenheit");
		rdbtnKelvin = new JRadioButton("Kelvin");
		buttonGroup = new ButtonGroup();

		lblCity = new JLabel("City");
		lblCountry = new JLabel("Country");
		lblCoordinatesSearch = new JLabel("Search By Lon/Lat");
		lblLat = new JLabel("Latitude");
		lblLon = new JLabel("Longitude");

		comboBox = new JComboBox<>(EForecastOptions.values());

		showWeatherButton = new JButton("Show Weather");
		logoIntro = new JTextPane();
		defaultLogoAnimated = new ImageIcon(getClass().getResource("weather.gif"));
		webMapBtn = new JButton("Web Map");

		locationPanel = new JPanel();
	}

	/**
	 * Implementing Frame Components.
	 */
	private void startGUI()
	{
		setResizable(false);
		setName("weatherFrame");
		setTitle("Weather");
		setMinimumSize(new Dimension(1000, 750));
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 724);

		separatorOne.setBounds(3, 148, 135, 3);
		separatorOne.setBorder(new EtchedBorder(Color.GRAY, Color.LIGHT_GRAY));
		seperatorTwo.setBounds(3, 171, 135, 3);
		seperatorTwo.setBorder(new EtchedBorder(Color.GRAY, Color.LIGHT_GRAY));
		showWeatherButton.setBounds(846, 643, 132, 66);
		showWeatherButton.setEnabled(false);

		Image tImage;
		try
		{
			tImage = ImageIO.read(getClass().getResource("spark.jpg"));
		} catch (final IOException e)
		{
			tImage = null;
		}
		if (tImage != null) {
			contentPane.setIcon(new ImageIcon(tImage));
		}
		contentPane.setLayout(null);
		setContentPane(contentPane);

		layeredPane.setBounds(0, 11, 840, 698);
		layeredPane.setLayout(null);
		layeredPane.setOpaque(false);

		dayTabsPanel.setBounds(0, 11, 840, 440);
		dayTabsPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		dayTabsPanel.setVisible(false);
		dayTabsPanel.setForeground(Color.WHITE);
		dayTabsPanel.setBackground(new Color(225, 225, 225, 150));
		UIManager.put("TabbedPane.background", ColorUIResource.LIGHT_GRAY);
		UIManager.put("TabbedPane.contentAreaColor ", ColorUIResource.WHITE);
		UIManager.put("TabbedPane.selected", ColorUIResource.LIGHT_GRAY);
		UIManager.put("TabbedPane.shadow", ColorUIResource.WHITE);

		graphPanel.setVisible(false);
		graphPanel.setLayout(new BorderLayout(0, 0));
		graphPanel.setBounds(0, 457, 840, 241);

		locationPanel.setLayout(null);
		locationPanel.setBorder(new EtchedBorder(Color.GRAY, Color.LIGHT_GRAY));
		locationPanel.setOpaque(false);
		locationPanel.setBounds(843, 190, 143, 320);

		currentWeatherPanel.setBounds(1, 1, 840, 700);
		currentWeatherPanel.setLayout(null);
		currentWeatherPanel.setBackground(new Color(51, 73, 95));

		countryCheckBox.setBounds(5, 80, 21, 21);
		countryCheckBox.setOpaque(false);

		rdbtnMetric.setBounds(848, 46, 130, 23);
		rdbtnMetric.setSelected(true);
		buttonGroup.add(rdbtnMetric);
		rdbtnImperial.setBounds(848, 93, 130, 23);
		buttonGroup.add(rdbtnImperial);
		rdbtnKelvin.setBounds(848, 69, 130, 23);
		buttonGroup.add(rdbtnKelvin);
		rdbtnMetric.setOpaque(false);
		rdbtnMetric.setForeground(Color.WHITE);
		rdbtnImperial.setOpaque(false);
		rdbtnImperial.setForeground(Color.WHITE);
		rdbtnKelvin.setOpaque(false);
		rdbtnKelvin.setForeground(Color.WHITE);

		lblCity.setBounds(10, 10, 39, 14);
		lblCity.setForeground(Color.WHITE);
		lblCountry.setBounds(30, 85, 102, 14);
		lblCountry.setForeground(Color.WHITE);
		lblCoordinatesSearch.setBounds(27, 155, 150, 14);
		lblCoordinatesSearch.setForeground(Color.WHITE);
		lblLat.setBounds(10, 255, 102, 14);
		lblLat.setForeground(Color.WHITE);
		lblLon.setBounds(10, 190, 102, 14);
		lblLon.setForeground(Color.WHITE);

		latitudeText.setBounds(5, 270, 132, 31);
		latitudeText.setEditable(false);
		latitudeText.setBackground(Color.GRAY);

		longitudeText.setBounds(5, 205, 132, 31);
		longitudeText.setEditable(false);
		longitudeText.setBackground(Color.GRAY);

		comboBox.setBounds(848, 140, 132, 28);
		comboBox.setBackground(new Color(255, 255, 255));
		comboBox.setModel(new DefaultComboBoxModel<>(EForecastOptions.values()));

		countryName.setBounds(5, 100, 132, 31);
		countryName.setEditable(false);
		countryName.setBackground(Color.GRAY);

		cityName.setBounds(5, 25, 132, 31);

		logoIntro.setBackground(new Color(51, 73, 95));
		logoIntro.setBounds(0, 0, 835, 672);
		logoIntro.insertIcon(defaultLogoAnimated);
		logoIntro.setEditable(false);

		searchTypeCheckBox.setBounds(5, 150, 21, 21);
		searchTypeCheckBox.setOpaque(false);

		locationPanel.add(separatorOne);
		locationPanel.add(seperatorTwo);
		locationPanel.add(lblCity);
		locationPanel.add(cityName);
		locationPanel.add(lblCountry);
		locationPanel.add(countryCheckBox);
		locationPanel.add(countryName);
		locationPanel.add(searchTypeCheckBox);
		locationPanel.add(lblCoordinatesSearch);
		locationPanel.add(lblLat);
		locationPanel.add(lblLon);
		locationPanel.add(latitudeText);
		locationPanel.add(longitudeText);

		currentWeatherPanel.add(logoIntro);
		layeredPane.add(currentWeatherPanel);
		layeredPane.add(graphPanel);
		layeredPane.add(dayTabsPanel);
		contentPane.add(layeredPane);
		contentPane.add(rdbtnKelvin);
		contentPane.add(rdbtnImperial);
		contentPane.add(rdbtnMetric);
		contentPane.add(comboBox);
		contentPane.add(webMapBtn);
		contentPane.add(locationPanel);
		contentPane.add(showWeatherButton);

		webMapBtn.setEnabled(false);
		webMapBtn.setBounds(846, 539, 132, 39);
		setEvents();
		setVisible(true);
		cityName.requestFocus();
	}

	/**
	 * Current Forecast Initialize.
	 */
	private void currentDataInitialize()
	{
		// Icon
		curentDayIconInitialize();

		// Location Label
		locationLabelInitialize();

		// Forecast Information
		forcastInfoTableInitialize();

		// Humidity panel
		humidityPanelInitialize();

		// Clouds Panel
		cloudsPanelInitialize();

		// Thermometer Panel
		thermometerPanelInitialize();

	}

	/**
	 * Current Weather day icon initialize.
	 */
	private void curentDayIconInitialize()
	{
		final JTextPane iconPane = new JTextPane();
		iconPane.setOpaque(false);
		iconPane.setBounds(10, 11, 179, 126);
		iconPane.setEditable(false);

		try
		{
			final String symbolStr = currentDayData.getSymbol().getVar();
			final Image img = ((OpenWeatherMap) chosenService).getOpenWeatherMapSymbolIcon(symbolStr)
					.getScaledInstance(126, 126, java.awt.Image.SCALE_FAST);

			iconPane.insertIcon(new ImageIcon(img));
		} catch (final WeatherDataServiceException e)
		{

			if (e.getInnerExeption() instanceof IOException)
			{
				Image defaultImage;
				try
				{
					defaultImage = ImageIO.read(getClass().getResource("NoImage.png")).getScaledInstance(126, 126,
							java.awt.Image.SCALE_FAST);
				} catch (final IOException e1)
				{
					defaultImage = null;
				}
				if (defaultImage != null) {
					iconPane.insertIcon(new ImageIcon(defaultImage));
				}

			}
		}

		currentWeatherPanel.add(iconPane);
	}

	/**
	 * Current Weather Location label initialize.
	 */
	private void locationLabelInitialize()
	{
		final JLabel locationLabel = new JLabel(
				currentDayData.getLocation().getCityName() + ", " + currentDayData.getLocation().getCountryName());
		locationLabel.setBounds(210, 35, 373, 45);
		locationLabel.setFont(new Font("Arial", Font.BOLD, 25));
		currentWeatherPanel.add(locationLabel);
	}

	/**
	 * Current Weather Forecast info table initialize.
	 */
	private void forcastInfoTableInitialize()
	{
		final ZebraTable weatherDataTable = new ZebraTable();
		weatherDataTable.setModel(new DefaultTableModel(8, 2));
		weatherDataTable.setRowHeight(27);
		weatherDataTable.setEnabled(false);
		weatherDataTable.setBorder(new EtchedBorder(Color.GRAY, Color.BLACK));
		weatherDataTable.setSelectionBackground(Color.BLACK);
		weatherDataTable.setBackground(Color.WHITE);
		weatherDataTable.getColumnModel().getColumn(1).setPreferredWidth(300);
		weatherDataTable.setFont(new Font("Arial", Font.BOLD, 20));
		weatherDataTable.setBounds(10, 148, 573, 216);

		// Day Temperature Tab Row
		weatherDataTable.setValueAt(" Day Temp. ", 0, 0);
		weatherDataTable.setValueAt("MinMax[ " + currentDayData.getTemperature().getMin() + " , "
				+ currentDayData.getTemperature().getMax() + " ]", 0, 1);

		// Precipitation Tab Row
		weatherDataTable.setValueAt(" Precipitation ", 1, 0);
		weatherDataTable
				.setValueAt(
						currentDayData.getPrecipitation().getType().equals("no")
								? currentDayData.getPrecipitation().getType()
								: (currentDayData.getPrecipitation().getType()
										+ currentDayData.getPrecipitation().getValue() + " "
										+ currentDayData.getPrecipitation().getUnits()),
						1, 1);

		// Wind Direction Tab Row
		weatherDataTable.setColumnSelectionAllowed(false);
		weatherDataTable.setValueAt(" Wind Direction ", 2, 0);
		weatherDataTable.setValueAt(currentDayData.getWindDirection().getName() + " "
				+ currentDayData.getWindDirection().getDegValue() + " " + currentDayData.getWindDirection().getCode(),
				2, 1);

		// Wind Speed Tab Row
		weatherDataTable.setValueAt(" Wind Speed ", 3, 0);
		weatherDataTable.setValueAt(
				currentDayData.getWindSpeed().getMpsValue() + " mps  " + currentDayData.getWindSpeed().getName(), 3, 1);

		// Pressure Tab Row
		weatherDataTable.setValueAt(" Pressure ", 4, 0);
		weatherDataTable.setValueAt(
				currentDayData.getPressure().getValue() + " " + currentDayData.getPressure().getUnit(), 4, 1);

		// Sun Rise and Set Tab Row
		final SimpleDateFormat outputSunTime = new SimpleDateFormat("HH:mm:ss");
		weatherDataTable.setValueAt(" SUN ", 5, 0);
		weatherDataTable.setValueAt("Rise [ " + outputSunTime.format(currentDayData.getSun().getRise()) + " ] Set : [ "
				+ outputSunTime.format(currentDayData.getSun().getSet()) + " ]", 5, 1);

		// Geo Coordinates Tab Row
		weatherDataTable.setValueAt(" Geo Coord. ", 6, 0);
		weatherDataTable.setValueAt("[" + currentDayData.getLocation().getGeoLocation().getLongitude() + " , "
				+ currentDayData.getLocation().getGeoLocation().getLatitude() + "]", 6, 1);

		// Last Update Weather Time Row
		final SimpleDateFormat outputUpdateTime = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
		weatherDataTable.setValueAt(" Last Update ", 7, 0);
		weatherDataTable.setValueAt(outputUpdateTime.format(currentDayData.getLastUpdate()), 7, 1);
		currentWeatherPanel.add(weatherDataTable);
	}

	/**
	 * Current Weather Humidity panel initialize.
	 */
	private void humidityPanelInitialize()
	{
		final JPanel humidityPanel = new JPanel();
		humidityPanel.setBounds(303, 399, 280, 280);
		humidityPanel.setLayout(new GridLayout(1, 0, 0, 0));
		humidityPanel.setOpaque(false);
		humidityPanel.add(dialPlotIndtialization(currentDayData.getHumidity().getValue(),
				currentDayData.getHumidity().getUnit()));
		final Border humidityBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Humidity ",
				TitledBorder.TOP, 2, new Font("Italic", Font.BOLD, 13), Color.BLACK);
		humidityPanel.setBorder(humidityBorder);
		currentWeatherPanel.add(humidityPanel);
	}

	/**
	 * Current Weather Clouds panel initialize.
	 */
	private void cloudsPanelInitialize()
	{
		final JPanel cloudsPanel = new JPanel();
		cloudsPanel.setBounds(3, 399, 280, 280);
		cloudsPanel.setLayout(new GridLayout(1, 0, 0, 0));
		cloudsPanel.setOpaque(false);
		cloudsPanel.add(
				dialPlotIndtialization(currentDayData.getClouds().getValue(), currentDayData.getClouds().getUnit()));

		final Border cloudsBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Clouds: " + currentDayData.getClouds().getName(), TitledBorder.TOP, 2,
				new Font("Italic", Font.BOLD, 13), Color.BLACK);
		cloudsPanel.setBorder(cloudsBorder);
		currentWeatherPanel.add(cloudsPanel);
	}

	/**
	 * Current Weather Thermometer panel initialize.
	 */
	private void thermometerPanelInitialize()
	{
		final JPanel thermometerPanel = new JPanel();
		thermometerPanel.setLayout(new GridLayout(1, 0, 0, 0));
		thermometerPanel.setBounds(593, 11, 237, 678);
		thermometerPanel.setOpaque(false);
		thermometerPanel.add(thermometerInitialize(currentDayData.getTemperature().getCurrentTempValue()));
		currentWeatherPanel.add(thermometerPanel);
	}

	/**
	 * Daily Weather Forecast Initialize.
	 */
	private void dailyDataInitialize()
	{
		Time currentDay;
		final SimpleDateFormat output = new SimpleDateFormat("dd.MM.yy");
		graphDataset = new ArrayList<>();
		final DefaultCategoryDataset colsDataset = new DefaultCategoryDataset(),
				lineDataset = new DefaultCategoryDataset();
		double averageTemperature;
		dayTabsPanel.removeAll();
		final String location = "Location : " + forcastData.getLocation().getCityName() + ", "
				+ forcastData.getLocation().getCountryName();
		final String currentTemperature = "Temperature : " + currentDayData.getTemperature().getCurrentTempValue();
		final String currentHumidity = "Humidity : " + currentDayData.getHumidity().getValue() + " " + currentDayData.getHumidity().getUnit();
		final String unitSign = unitSelected == ETemperatureUnits.CELSIUS ? "\u00B0C"
				: unitSelected == ETemperatureUnits.FAHRENHEIT ? "\u00B0F" : "\u00B0K";

		for (int index = 0; index < forcastDays; index++)
		{

			currentDay = forcastData.getForcastList().get(index);
			averageTemperature = (currentDay.getTemperature().getMax() + currentDay.getTemperature().getMin()) / 2;
			currentDayPanel = new JPanel();
			currentDayPanel.setLayout(null);
			currentDayPanel.setOpaque(false);
			dayTabsPanel.addTab(output.format(currentDay.getDay()), null, currentDayPanel, null);

			// Graph data for current day
			colsDataset.addValue(currentDay.getTemperature().getMax(), "Maximum", output.format(currentDay.getDay()));
			colsDataset.addValue(currentDay.getTemperature().getMin(), "Minimum", output.format(currentDay.getDay()));
			graphDataset.add(colsDataset);
			lineDataset.addValue(averageTemperature, "Averege Time Line", output.format(currentDay.getDay()));
			graphDataset.add(lineDataset);

			// Current Weather and Icon Panel
			iconPanelInitialize(currentDay, unitSign);

			// Day info Table
			dayInfoTableInitialize(currentDay);

			// Current Day Humidity
			dayHumidityInitialize(currentDay);

			// Current Day Cloudiness
			dayCloudnesInitialize(currentDay);

			// Real time Weather
			realTimeVeatherInitialize(location, currentTemperature, currentHumidity, unitSign);

		}

		dayTabsPanel.setUI(new BasicTabbedPaneUI()
		{
			@Override
			protected int calculateTabWidth(final int tabPlacement, final int tabIndex, final FontMetrics metrics)
			{
				final int week = 7;
				final int twoWeeks = 14;
				int tabSize = 0;
				if (forcastDays == week)
				{
					tabSize = 118;
				} else if (forcastDays == twoWeeks)
				{
					tabSize = 100;
				}
				return tabSize;
			}

		});

	}

	/**
	 * Daily Weather Icon panel initialize.
	 *
	 * @param currentDay
	 *            the current day data
	 * @param unitSign
	 *            the icon sign
	 */
	private void iconPanelInitialize(final Time currentDay, final String unitSign)
	{
		final JPanel weatherAndIconPanel = new JPanel();
		weatherAndIconPanel.setBounds(380, 15, 250, 120);
		weatherAndIconPanel.setLayout(null);
		weatherAndIconPanel.setOpaque(false);
		final Border weatherAndIconBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Temperature", TitledBorder.TOP, 2, new Font("Italic", Font.BOLD, 15), Color.BLACK);
		weatherAndIconPanel.setBorder(weatherAndIconBorder);
		weatherAndIconPanel.add(dayTemperatureTableInitialize(currentDay, unitSign));
		weatherAndIconPanel.add(dailyWeatherIconInitialize(currentDay));
		currentDayPanel.add(weatherAndIconPanel);
	}

	/**
	 * Daily Weather icon initialize.
	 *
	 * @param currentDay
	 *            the current day
	 * @return the JTextPane with icon
	 */
	private JTextPane dailyWeatherIconInitialize(final Time currentDay)
	{
		final JTextPane textPane = new JTextPane();
		textPane.setOpaque(false);
		try
		{
			final String symbolStr = currentDay.getSymbol().getVar();
			final Image img = ((OpenWeatherMap) chosenService).getOpenWeatherMapSymbolIcon(symbolStr)
					.getScaledInstance(80, 80, java.awt.Image.SCALE_FAST);
			textPane.insertIcon(new ImageIcon(img));

		} catch (final WeatherDataServiceException e)
		{
			Image defaultImage;
			try
			{
				defaultImage = ImageIO.read(getClass().getResource("NoImage.png")).getScaledInstance(80, 80,
						java.awt.Image.SCALE_FAST);
			} catch (final IOException e1)
			{
				defaultImage = null;
			}
			if (defaultImage != null) {
				textPane.insertIcon(new ImageIcon(defaultImage));
			}
		}
		textPane.setBounds(13, 30, 78, 78);
		textPane.setEditable(false);
		textPane.setBackground(null);
		return textPane;
	}

	/**
	 * Daily Weather temperature table initialize.
	 *
	 * @param currentDay
	 *            the current day data
	 * @param unitSign
	 *            the unit sign
	 * @return the table with temperature for current day
	 */
	private JTable dayTemperatureTableInitialize(final Time currentDay, final String unitSign)
	{
		final ZebraTable temperatureTable = new ZebraTable();

		temperatureTable.setEnabled(false);
		temperatureTable.setBounds(98, 22, 145, 92);
		temperatureTable.setModel(new DefaultTableModel(4, 2));
		temperatureTable.setRowHeight(23);
		temperatureTable.setColumnSelectionAllowed(false);

		// Morning Tab Row
		temperatureTable.setValueAt(" Morning ", 0, 0);
		temperatureTable.setValueAt(currentDay.getTemperature().getMorning() + unitSign, 0, 1);

		// Day Tab Row
		temperatureTable.setValueAt(" Day ", 1, 0);
		temperatureTable.setValueAt(currentDay.getTemperature().getDay() + unitSign, 1, 1);

		// Evening Tab Row
		temperatureTable.setValueAt(" Evening ", 2, 0);
		temperatureTable.setValueAt(currentDay.getTemperature().getEvening() + unitSign, 2, 1);

		// Night Tab Row
		temperatureTable.setValueAt(" Night ", 3, 0);
		temperatureTable.setValueAt(currentDay.getTemperature().getNight() + unitSign, 3, 1);

		temperatureTable.setBorder(new EtchedBorder());
		temperatureTable.setFont(new Font("Arial", Font.BOLD, 13));
		return temperatureTable;
	}

	/**
	 * Daily Weather info table initialize.
	 *
	 * @param currentDay
	 *            the current day data
	 */
	private void dayInfoTableInitialize(final Time currentDay)
	{
		final ZebraTable dayInfoTable = new ZebraTable();
		dayInfoTable.setEnabled(false);
		dayInfoTable.setBounds(10, 145, 620, 240);
		dayInfoTable.setModel(new DefaultTableModel(4, 2));
		dayInfoTable.setRowHeight(60);
		dayInfoTable.setBorder(new EtchedBorder(Color.GRAY, Color.BLACK));
		dayInfoTable.setSelectionBackground(Color.BLACK);
		dayInfoTable.setBackground(Color.WHITE);
		dayInfoTable.getColumnModel().getColumn(1).setPreferredWidth(300);
		dayInfoTable.setFont(new Font("Arial", Font.BOLD, 20));
		currentDayPanel.add(dayInfoTable);

		// Precipitation Tab Row
		dayInfoTable.setValueAt(" Percipitation", 0, 0);
		if (currentDay.getPrecipitation() != null)
		{
			dayInfoTable.setValueAt(" " + currentDay.getPrecipitation().getType() + " "
					+ currentDay.getPrecipitation().getValue() + " mm", 0, 1);
		} else
		{
			dayInfoTable.setValueAt(" None", 0, 1);
		}

		// Wind Direction Tab Row
		dayInfoTable.setValueAt(" Wind Direction", 1, 0);
		dayInfoTable.setValueAt(" " + currentDay.getWindDirection().getName() + " "
				+ currentDay.getWindDirection().getDegValue() + " deg.", 1, 1);

		// Wind Speed Tab Row
		dayInfoTable.setValueAt(" Wind Speed", 2, 0);
		dayInfoTable.setValueAt(
				" " + currentDay.getWindSpeed().getMpsValue() + " mps " + currentDay.getWindSpeed().getName(), 2, 1);

		// Pressure Tab Row
		dayInfoTable.setValueAt(" Pressure", 3, 0);
		dayInfoTable.setValueAt(" " + currentDay.getPressure().getValue() + " " + currentDay.getPressure().getUnit(), 3,
				1);
	}

	/**
	 * Daily Weather humidity initialize.
	 *
	 * @param currentDay
	 *            the current day data
	 */
	private void dayHumidityInitialize(final Time currentDay)
	{
		final JPanel humidityPanel = new JPanel();
		humidityPanel.setBounds(650, 205, 180, 180);
		humidityPanel.setLayout(new GridLayout(1, 0, 0, 0));
		humidityPanel.setOpaque(false);
		humidityPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		humidityPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		humidityPanel
				.add(dialPlotIndtialization(currentDay.getHumidity().getValue(), currentDay.getHumidity().getUnit()));
		final Border humidityBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Humidity",
				TitledBorder.TOP, 2, new Font("Italic", Font.BOLD, 13), Color.BLACK);
		humidityPanel.setBorder(humidityBorder);
		currentDayPanel.add(humidityPanel);
	}

	/**
	 * Daily Weather cloudiness initialize.
	 *
	 * @param currentDay
	 *            the current day data
	 */
	private void dayCloudnesInitialize(final Time currentDay)
	{
		final JPanel cloudsPanel = new JPanel();
		cloudsPanel.setBounds(650, 13, 180, 180);
		cloudsPanel.setLayout(new GridLayout(1, 0, 0, 0));
		cloudsPanel.setOpaque(false);
		cloudsPanel.add(dialPlotIndtialization(currentDay.getClouds().getValue(), currentDay.getClouds().getUnit()));
		final Border cloudsBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Clouds: " + currentDay.getClouds().getName(), TitledBorder.TOP, 2,
				new Font("Italic", Font.BOLD, 13), Color.BLACK);
		cloudsPanel.setBorder(cloudsBorder);
		currentDayPanel.add(cloudsPanel);
	}

	/**
	 * Real time weather initialize On Daily Weather panel.
	 *
	 * @param location
	 *            the weather location
	 * @param currentTemperature
	 *            the current temperature
	 * @param currentHumidity
	 *            the current humidity
	 * @param unitSign
	 *            the unit sign
	 */
	private void realTimeVeatherInitialize(final String location, final String currentTemperature,
			final String currentHumidity, final String unitSign)
	{

		final JPanel realTimePanel = new JPanel();
		realTimePanel.setOpaque(false);
		final JLabel locationLable = new JLabel(location);
		locationLable.setOpaque(false);
		final JLabel currentTemperatureLable = new JLabel(currentTemperature + unitSign);
		currentTemperatureLable.setOpaque(false);
		final JLabel currentHumidityLable = new JLabel(currentHumidity);
		currentHumidityLable.setOpaque(false);

		realTimePanel.setBounds(10, 13, 350, 120);
		realTimePanel.setLayout(null);
		final Border realTimeBorder = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(new Color(33, 228, 22), Color.LIGHT_GRAY), "Now", TitledBorder.TOP, 2,
				new Font("Italic", Font.BOLD, 20), new Color(33, 228, 22));
		realTimePanel.setBorder(realTimeBorder);
		locationLable.setBounds(10, 25, 350, 20);
		locationLable.setFont(new Font("Arial", Font.BOLD, 16));
		currentTemperatureLable.setBounds(10, 55, 350, 20);
		currentTemperatureLable.setFont(new Font("Arial", Font.BOLD, 16));
		currentHumidityLable.setBounds(10, 85, 350, 20);
		currentHumidityLable.setFont(new Font("Arial", Font.BOLD, 16));
		realTimePanel.add(locationLable);
		realTimePanel.add(currentTemperatureLable);
		realTimePanel.add(currentHumidityLable);
		currentDayPanel.add(realTimePanel);
	}

	/**
	 * Dial plot initialization.
	 *
	 * @param value
	 *            the value to see on plot
	 * @param unit
	 *            the unit
	 * @return the chart panel
	 */
	private ChartPanel dialPlotIndtialization(final double value, final String unit)
	{
		final DefaultValueDataset dataset = new DefaultValueDataset(value);
		final DialPlot plot = new DialPlot(dataset);
		plot.setDialFrame(new StandardDialFrame());
		plot.addLayer(new DialTextAnnotation(value + unit));
		plot.addLayer(new DialPointer.Pointer());
		plot.addLayer(new StandardDialRange(0, 80, Color.blue));
		plot.addLayer(new StandardDialRange(80, 100, Color.red));
		final StandardDialScale scale = new StandardDialScale(0, 100, -120D, -300D, 10D, 10);
		scale.setTickRadius(0.88D);
		scale.setTickLabelOffset(0.180D);
		scale.setTickLabelFont(new Font("Dialog", 0, 14));
		plot.addScale(0, scale);
		final JFreeChart chart = new JFreeChart(plot);
		chart.setBackgroundPaint(null);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setOpaque(false);
		chartPanel.setPopupMenu(null);
		return chartPanel;
	}

	/**
	 * GUI exception handling.
	 *
	 * @param ex
	 *            the exception
	 * @param window
	 *            the window that pop up notification that belongs to him
	 */
	private void exceptionHandlingGUI(final Throwable ex, final Window window)
	{
		final WeatherDataServiceException exeption = (WeatherDataServiceException) ex;
		if (exeption.getInnerExeption() instanceof IOException)
		{

			JOptionPane.showMessageDialog(window, "Check Connection", "ooops", JOptionPane.ERROR_MESSAGE);

		} else if (exeption.getInnerExeption() == null)
		{
			JOptionPane.showMessageDialog(window, exeption.getExMsg(), "ooops", JOptionPane.ERROR_MESSAGE);

		} else if (exeption.getInnerExeption() instanceof SAXException)
		{
			JOptionPane.showMessageDialog(window, "Inaccessible  Location !", "ooops", JOptionPane.ERROR_MESSAGE);

		} else if (exeption.getInnerExeption() instanceof NullPointerException
				|| exeption.getInnerExeption() instanceof NumberFormatException)
		{
			JOptionPane.showMessageDialog(window, exeption.getExMsg(), "ooops", JOptionPane.ERROR_MESSAGE);
		} else
		{
			ex.printStackTrace();
		}
	}

	/**
	 * Sets the layered pane to view Current Weather or Daily Weather.
	 *
	 * @param isCanRefreshGUI
	 *            if GUI can refresh
	 */
	private void setLayeredPane(final boolean isCanRefreshGUI)
	{
		if (isCanRefreshGUI)
		{
			graphPanel.removeAll();
			dayTabsPanel.removeAll();
			currentWeatherPanel.removeAll();
			if (forcastDays != 1)
			{
				graphPanel.setVisible(true);
				dayTabsPanel.setVisible(true);
				currentWeatherPanel.setVisible(false);
				dailyDataInitialize();
				graphInitialize(graphDataset);
			} else
			{
				forcastData = null;
				graphPanel.setVisible(false);
				dayTabsPanel.setVisible(false);
				currentWeatherPanel.setVisible(true);
				currentWeatherPanel.setBackground(new Color(200, 221, 242));
				currentDataInitialize();
				currentWeatherPanel.repaint();
			}
			webMapBtn.setEnabled(true);

		}
	}

	/**
	 * Gets the selected button from buttonGroup to get chosen temperature
	 * units.
	 *
	 * @param group
	 *            the temperature units button group
	 * @return the selected temperature unit
	 */
	private ETemperatureUnits getSelectedButton(final ButtonGroup group)
	{
		String buttonName = null;
		ETemperatureUnits selectedUnit = null;
		for (final Enumeration<AbstractButton> buttons = group.getElements(); buttons.hasMoreElements();)
		{
			final AbstractButton button = buttons.nextElement();
			if (button.isSelected())
			{
				buttonName = button.getText();
				break;
			}
		}
		if (buttonName != null) {
			switch (buttonName)
            {
            case "Celsius":
                selectedUnit = ETemperatureUnits.CELSIUS;
                break;

            case "Fahrenheit":
                selectedUnit = ETemperatureUnits.FAHRENHEIT;
                break;

            case "Kelvin":
                selectedUnit = ETemperatureUnits.KELVIN;
                break;

            default:
                break;
            }
		}
		return selectedUnit;
	}

	/**
	 * Minimum, Maximum, Average Temperature Graph.
	 *
	 * @param dailyTemperatureData
	 *            the daily temperature data
	 */
	private void graphInitialize(final List<DefaultCategoryDataset> dailyTemperatureData)
	{
		final JFreeChart averageWeatherChart = ChartFactory.createBarChart("Average Weather", "Category",
				unitSelected.name(), dailyTemperatureData.get(0), PlotOrientation.VERTICAL, true, true, false);
		averageWeatherChart.setBackgroundPaint(null);

		final CategoryPlot plot = averageWeatherChart.getCategoryPlot();
		plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
		plot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		plot.setRangeGridlinePaint(Color.BLACK);
		plot.setDataset(1, dailyTemperatureData.get(1));
		plot.mapDatasetToRangeAxis(1, 0);

		final CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
		final ValueAxis axis2 = new NumberAxis();
		axis2.setAxisLineVisible(false);
		axis2.setTickLabelsVisible(false);
		axis2.setTickMarksVisible(false);
		plot.setRangeAxis(1, axis2);

		final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
		renderer2.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
		plot.setRenderer(1, renderer2);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);

		final ChartPanel everageTemperatureChartPanel = new ChartPanel(averageWeatherChart);
		everageTemperatureChartPanel.setPopupMenu(null);
		everageTemperatureChartPanel.setMouseWheelEnabled(false);
		everageTemperatureChartPanel.setMouseZoomable(false);
		everageTemperatureChartPanel.setDomainZoomable(false);
		everageTemperatureChartPanel.setRangeZoomable(false);
		everageTemperatureChartPanel.setOpaque(false);
		everageTemperatureChartPanel.setBorder(new EtchedBorder(Color.WHITE, Color.WHITE));

		graphPanel.removeAll();
		graphPanel.add(everageTemperatureChartPanel);
		graphPanel.setBackground(new Color(200, 221, 242));

	}

	/**
	 * Sets the Thermometer Range by Chosen unit .
	 */
	private void setThermometerByUnitInitialize()
	{
		double low = 0, med = 0, hight = 0, upperBound = 0;
		switch (unitSelected)
		{
		case CELSIUS:
			med = 30.0;
			hight = 40.0;
			upperBound = 60;
			break;

		case FAHRENHEIT:
			low = 32.0;
			med = 86.0;
			hight = 104.0;
			upperBound = 140.0;
			break;

		case KELVIN:
			low = 273.15;
			med = 303.15;
			hight = 313.15;
			upperBound = 373.15;
			break;

		default:
			break;
		}

		plot.setSubrange(0, low, med);
		plot.setSubrange(1, med + 0.1, hight);
		plot.setSubrange(2, hight + 0.1, 1000.0);

		plot.setDisplayRange(0, 0, med);
		plot.setDisplayRange(1, med + 0.1, hight);
		plot.setDisplayRange(2, hight + 0.1, 99.0);

		plot.setSubrangePaint(0, Color.GREEN);
		plot.setSubrangePaint(1, Color.ORANGE);
		plot.setSubrangePaint(2, Color.RED);

		plot.setUpperBound(upperBound);

	}

	/**
	 * Thermometer initialize.
	 *
	 * @param value
	 *            temperature value
	 * @return the Thermometer Chart panel
	 */
	private ChartPanel thermometerInitialize(final double value)
	{

		final int selectedUnit = unitSelected == ETemperatureUnits.CELSIUS ? ThermometerPlot.UNITS_CELCIUS
				: unitSelected == ETemperatureUnits.FAHRENHEIT ? ThermometerPlot.UNITS_FAHRENHEIT
						: ThermometerPlot.UNITS_KELVIN;
		final DefaultValueDataset dataset = new DefaultValueDataset(value);
		plot = new ThermometerPlot(dataset);
		final JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
		chart.setBackgroundPaint(null);
		plot.setThermometerStroke(new BasicStroke(0.5f));
		plot.zoom(150);
		plot.setThermometerPaint(Color.BLACK);
		plot.setBackgroundPaint(null);
		plot.setValuePaint(Color.BLACK);
		plot.setOutlinePaint(null);
		plot.setUnits(selectedUnit);
		plot.setBulbRadius(70);
		setThermometerByUnitInitialize();

		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setOpaque(false);
		chartPanel.setPopupMenu(null);
		return chartPanel;
	}

	/**
	 * Sets the events.
	 */
	private void setEvents()
	{
		setShowWeatherBtnEvent();
		setCityNameTextFieldEvent();
		setCountryNameTextFieldEvent();
		setRadioBtnsEvent();
		setComboBoxEvent();
		setWebMapEvent();
		setCountryCheckBoxEvent();
		setLatitudeTextFieldEvent();
		setLongitudeTextFieldEvent();
		setSearchTypeCheckBoxEvent();
	}

	/**
	 * Sets the country name text field event.
	 */
	private void setCountryNameTextFieldEvent()
	{
		// Enter key event to country name Text Field
		countryName.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent evt)
			{
				if (evt.getKeyCode() == KeyEvent.VK_ENTER && !cityName.getText().equals(""))
				{
					showWeatherButton.doClick();
				}
			}
		});
	}

	/**
	 * Sets the radio buttons event.
	 */
	private void setRadioBtnsEvent()
	{
		// Enter key event to Radio buttons
		final Enumeration<AbstractButton> buttons = buttonGroup.getElements();
		while (buttons.hasMoreElements())
		{

			buttons.nextElement().addKeyListener(new KeyAdapter()
			{
				@Override
				public void keyPressed(final KeyEvent evt)
				{
					if (evt.getKeyCode() == KeyEvent.VK_ENTER && !cityName.getText().equals("") || (!longitudeText.getText().equals("") && !latitudeText.getText().equals("")))
					{
						showWeatherButton.doClick();
					}
				}
			});
		}
	}

	/**
	 * Sets the Country check box event.
	 */
	private void setCountryCheckBoxEvent()
	{

		// Country Check Box event to Enable/Disable country name Text Field
		countryCheckBox.addActionListener(arg -> {
			{
				if (((JCheckBox) (arg.getSource())).isSelected())
				{
					countryName.setEditable(true);
					countryName.setBackground(Color.WHITE);
				} else
				{
					countryName.setText(null);
					countryName.setEditable(false);
					countryName.setBackground(Color.GRAY);

				}
			}
		});

		// Enter key event to Country Check Box
		countryCheckBox.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent evt)
			{
				if (evt.getKeyCode() == KeyEvent.VK_ENTER && !cityName.getText().equals(""))
				{
					showWeatherButton.doClick();
				}
			}
		});

	}

	/**
	 * Sets the show weather button event.
	 */
	private void setShowWeatherBtnEvent()
	{
		final JFrame mainFrame = this;
		class showWeatherEvent implements ActionListener
		{

			@Override
			public void actionPerformed(final ActionEvent arg0)
			{
				final JButton btn = ((JButton) arg0.getSource());

				// intended to prevent another action while loading data to UI
				btn.removeActionListener(this);
				btn.setIcon(new ImageIcon(getClass().getResource("loadingGif.gif")));
				webMapBtn.setEnabled(false);

				new Thread(()->
				{

						String city = null, country = null;
						String lon = null, lat = null;
						boolean isCanRefreshGUI = true;
						forcastDays = ((EForecastOptions) comboBox.getSelectedItem()).days;
						if (searchTypeCheckBox.isSelected())
						{
							lon = longitudeText.getText();
							lat = latitudeText.getText();
						} else
						{

							city = cityName.getText();
							if (countryName.getText() != null)
							{
								country = countryName.getText();
							}

						}
						try
						{

							chosenService = WeatherDataServiceFactory
									.getOrCreateWeatherDataService(WeatherDataServiceFactory.OPENWEATHERMAP);
							{

								unitSelected = getSelectedButton(buttonGroup);
								WeatherLocation wantedLocation = new WeatherLocation(city, country, lat, lon);
								if (forcastDays != 1)
								{
									forcastData = (WeatherDataDailyForecast) chosenService
											.getWeatherDataDailyForecast(wantedLocation, forcastDays, unitSelected);
								}
								currentDayData = (WeatherDataCurrentDay) chosenService
										.getWeatherDataCurrentDay(wantedLocation, unitSelected);
								wantedLocation = null;

							}
						} catch (final WeatherDataServiceException e)
						{
							exceptionHandlingGUI(e, mainFrame);

							isCanRefreshGUI = false;
						}

						setLayeredPane(isCanRefreshGUI);
						((JButton) arg0.getSource()).addActionListener(new showWeatherEvent());
						webMapBtn.setEnabled(true);
						btn.setIcon(null);

				}).start();

			}

		}

		showWeatherButton.addActionListener(new showWeatherEvent());

	}

	/**
	 * Sets the city name text field event.
	 */
	private void setCityNameTextFieldEvent()
	{
		// City name Text Box event to Enable/Disable Show Weather Button
		cityName.getDocument().addDocumentListener(new DocumentListener()
		{

			@Override
			public void changedUpdate(final DocumentEvent arg0)
			{
			}

			@Override
			public void insertUpdate(final DocumentEvent arg0)
			{
				showWeatherButton.setEnabled(true);

			}

			@Override
			public void removeUpdate(final DocumentEvent arg0)
			{
				if (cityName.getText().length() == 0)
				{
					showWeatherButton.setEnabled(false);
				}
			}

		});

		// Enter key event to City name Text Box
		cityName.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent evt)
			{
				if (evt.getKeyCode() == KeyEvent.VK_ENTER && !cityName.getText().equals(""))
				{
					showWeatherButton.doClick();
				}
			}
		});

	}

	/**
	 * Sets the Latitude text field event.
	 */
	private void setLatitudeTextFieldEvent()
	{
		// Latitude Text Box event to Enable/Disable Show Weather Button
		latitudeText.getDocument().addDocumentListener(new DocumentListener()
		{

			@Override
			public void removeUpdate(final DocumentEvent e)
			{

				if (latitudeText.getText().isEmpty() || longitudeText.getText().isEmpty())
				{
					showWeatherButton.setEnabled(false);
				}
			}

			@Override
			public void insertUpdate(final DocumentEvent e)
			{
				if (!latitudeText.getText().isEmpty() && !longitudeText.getText().isEmpty())
				{
					showWeatherButton.setEnabled(true);
				}

			}

			@Override
			public void changedUpdate(final DocumentEvent e)
			{

			}
		});

		// Enter key event to Latitude Text Box
		latitudeText.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent evt)
			{
				if (evt.getKeyCode() == KeyEvent.VK_ENTER && !latitudeText.getText().isEmpty()
						&& !longitudeText.getText().isEmpty())
				{
					showWeatherButton.doClick();
				}
			}
		});
	}

	/**
	 * Sets the Longitude text field event.
	 */
	private void setLongitudeTextFieldEvent()
	{
		// Longitude Text Box event to Enable/Disable Show Weather Button
		longitudeText.getDocument().addDocumentListener(new DocumentListener()
		{

			@Override
			public void removeUpdate(final DocumentEvent e)
			{

				if (latitudeText.getText().isEmpty() || longitudeText.getText().isEmpty())
				{
					showWeatherButton.setEnabled(false);
				}
			}

			@Override
			public void insertUpdate(final DocumentEvent e)
			{
				if (!latitudeText.getText().isEmpty() && !longitudeText.getText().isEmpty())
				{
					showWeatherButton.setEnabled(true);
				}

			}

			@Override
			public void changedUpdate(final DocumentEvent e)
			{

			}
		});

		// Enter key event to Longitude Text Box
		longitudeText.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent evt)
			{
				if (evt.getKeyCode() == KeyEvent.VK_ENTER && !latitudeText.getText().isEmpty()
						&& !longitudeText.getText().isEmpty())
				{
					showWeatherButton.doClick();
				}
			}
		});
	}

	/**
	 * Sets the Search Type check box event.
	 */
	private void setSearchTypeCheckBoxEvent()
	{
		// Search Type Check Box event
		// To switch between city name country name and latitude , longitude
		// text fields
		searchTypeCheckBox.addActionListener((arg0)->
		{

				if (((JCheckBox) arg0.getSource()).isSelected())
				{
					latitudeText.setEditable(true);
					latitudeText.setBackground(Color.WHITE);
					longitudeText.setEditable(true);
					longitudeText.setBackground(Color.WHITE);
					longitudeText.requestFocus(true);
					cityName.setText("");
					countryName.setText("");
					cityName.setEditable(false);
					cityName.setBackground(Color.GRAY);
					countryName.setEditable(false);
					countryName.setBackground(Color.GRAY);
					countryCheckBox.setEnabled(false);

				} else
				{
					latitudeText.setEditable(false);
					latitudeText.setBackground(Color.GRAY);
					longitudeText.setEditable(false);
					longitudeText.setBackground(Color.GRAY);
					cityName.requestFocus(true);
					latitudeText.setText("");
					longitudeText.setText("");
					cityName.setEditable(true);
					cityName.setBackground(Color.WHITE);
					countryCheckBox.setEnabled(true);

				}
				countryCheckBox.setSelected(false);
		});

	}

	/**
	 * Sets the combo box event.
	 */
	private void setComboBoxEvent()
	{

		// Enter key event to comboBox
		comboBox.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent evt)
			{
				if (evt.getKeyCode() == KeyEvent.VK_ENTER && (!cityName.getText().equals("") || (!longitudeText.getText().equals("") && !latitudeText.getText().equals(""))))
				{
					showWeatherButton.doClick();
				}
			}
		});
	}

	/**
	 * Sets the web map event.
	 */
	private void setWebMapEvent()
	{
		webMapBtn.addActionListener((arg0)->
		{
				final double lat = currentDayData.getLocation().getGeoLocation().getLatitude();
				final double lon = currentDayData.getLocation().getGeoLocation().getLongitude();

				final String uriString ="http://www.openweathermap.org/Maps?zoom=12&layers=B0FTTFF&lat=" + lat + "&lon=" + lon;
				URI uri;

				try
				{
					uri = new URI(uriString);
					final Desktop desktop = Desktop.getDesktop();
					desktop.browse(uri);
				} catch (URISyntaxException | IOException e)
				{
					webMapBtn.setEnabled(false);
				}

		});
	}
}
