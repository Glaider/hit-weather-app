package il.ac.hit.weathergui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * The Class ZebraTable extends JTable.
 * </p>
 * Class Allows to Create JTable With Colored Rows
 * 
 * @author Stephan Sarkisyan , Alex Verevkine
 */
class ZebraTable extends JTable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
	{
		Component component = super.prepareRenderer(renderer, row, column);

		component.setBackground(Color.white);

		if (!isRowSelected(row))
		{
			component.setBackground(row % 2 == 0 ? getBackground() : new Color(225, 225, 225));
		}

		return component;
	}
}