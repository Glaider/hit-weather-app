package il.ac.hit.weathergui;

/**
 * The Enum EForecastOptions.
 * 
 * @author Stephan Sarkisyan , Alex Verevkine
 */
enum EForecastOptions
{

	/** The current. */
	CURRENT(1),

	/** The one week. */
	ONE_WEEK(7),

	/** The two weeks. */
	TWO_WEEKS(14);

	/** The days value. */
	public int days;

	/**
	 * Instantiates a new forecast options.
	 *
	 * @param num
	 *            the number of Days
	 */
	private EForecastOptions(int num)
	{
		days = num;
	}

}
